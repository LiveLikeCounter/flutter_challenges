import 'package:flutter/material.dart';

import 'challenge_bottom_navigation_bar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Flutter Demo', home: TabBarChallenge());
  }
}

class TabBarChallenge extends StatefulWidget {
  @override
  _TabBarChallengeState createState() => _TabBarChallengeState();
}

class _TabBarChallengeState extends State<TabBarChallenge>
    with SingleTickerProviderStateMixin {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF90A4CE),
      bottomNavigationBar: ChallengeBottomNavigationBar(),
    );
  }
}
