import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:outline_material_icons/outline_material_icons.dart';

import 'baritemmodel.dart';
import 'challenge_bottom_navigation_bar.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(title: 'Flutter Demo', home: FlutterTest());
  }
}

class FlutterTest extends StatefulWidget {
  @override
  _FlutterTestState createState() => _FlutterTestState();
}

class _FlutterTestState extends State<FlutterTest> {
  String animationName = 'idle';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          setState(() {
            if (animationName == "Activity")
              animationName = 'idle';
            else
              animationName = "Activity";
            print(animationName);
          });
        },
        child: Center(
          child: Container(
            child: FlareActor(
              "assets/home-bar-icon.flr",
              color: const Color(0xFF6367A2),
              animation: animationName,
            ),
          ),
        ),
      ),
    );
  }
}

class TabBarChallenge extends StatefulWidget {
  @override
  _TabBarChallengeState createState() => _TabBarChallengeState();
}

class _TabBarChallengeState extends State<TabBarChallenge>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF90A4CE),
      bottomNavigationBar: ChallengeBottomNavigationBar(),
    );
  }
}
