import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_sequence_animation/flutter_sequence_animation.dart';
import 'package:outline_material_icons/outline_material_icons.dart';
import 'package:rect_getter/rect_getter.dart';

import 'baritemmodel.dart';

class ChallengeBottomNavigationBar extends StatefulWidget {
  @override
  _ChallengeBottomNavigationBarState createState() =>
      _ChallengeBottomNavigationBarState();
}

class _ChallengeBottomNavigationBarState
    extends State<ChallengeBottomNavigationBar>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  static const double MAX_LIFT_SIZE  = 2;


  int selectedIndex = 0;
  int previousIndex = 0;

  SequenceAnimation _firstHalfRotationAnimation;
  SequenceAnimation _secondHalfRotationAnimation;

  Animation<double> _selectedSizeAnimation;
  Animation<double> _unSelectedSizeAnimation;

  @override
  void initState() {
    _controller =
        AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    _firstHalfRotationAnimation = SequenceAnimationBuilder()
        .addAnimatable(
            animatable: Tween<double>(begin: 0.0, end: -pi / 12),
            from: const Duration(milliseconds: 0),
            to: const Duration(milliseconds: 125),
            curve: Curves.easeInOut,
            tag: 'rotation')
        .addAnimatable(
            animatable: Tween<double>(begin: -pi / 12, end: 0.0),
            from: const Duration(milliseconds: 126),
            to: const Duration(milliseconds: 250),
            curve: Curves.easeInOut,
            tag: 'rotation')
        .animate(_controller);
    _secondHalfRotationAnimation = SequenceAnimationBuilder()
        .addAnimatable(
            animatable: Tween<double>(begin: 0.0, end: pi / 12),
            from: const Duration(milliseconds: 0),
            to: const Duration(milliseconds: 125),
            curve: Curves.easeInOut,
            tag: 'rotation')
        .addAnimatable(
            animatable: Tween<double>(begin: pi / 12, end: 0.0),
            from: const Duration(milliseconds: 126),
            to: const Duration(milliseconds: 250),
            curve: Curves.easeInOut,
            tag: 'rotation')
        .animate(_controller);

    _selectedSizeAnimation =
        Tween<double>(begin: 0, end: MAX_LIFT_SIZE).animate(_controller);
    _unSelectedSizeAnimation =
        Tween<double>(begin: MAX_LIFT_SIZE, end: 0).animate(_controller);
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);

    super.initState();
  }

  void _afterLayout(_) {
    onNavTap(0);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var barItems = <BottomNavigationBarItem>[];
    for (int i = 0; i < barModels.length; i++) {
      var barModel = barModels[i];
      barItems.add(new BottomNavigationBarItem(
          icon: AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return BarItemIcon(
                    unSelectedIcon: barModel.unSelectedIcon,
                    selectedIcon: barModel.selectedIcon,
                    unSelectedIconOpacity: barModel.unSelectedIconOpacity,
                    selectedIconOpacity: barModel.selectedIconOpacity,
                    iconPosKey: barModel.iconPosKey,
                    iconRotation: getAppropriateRotationValue(i),
                    liftSize: getAppropriateSizeValue(i));
              }),
          title: barModel.textWidget));
    }
    return Stack(
      children: [
        new BottomNavigationBar(
          backgroundColor: const Color(0xFF040407),
          type: BottomNavigationBarType.fixed,
          unselectedItemColor: Colors.grey,
          selectedItemColor: const Color(0xFF6367A2),
          currentIndex: selectedIndex,
          items: barItems,
          onTap: onNavTap,
        ),
        AnimatedPositioned(
          curve: Curves.easeInOut,
          duration: const Duration(milliseconds: 250),
          bottom: 10,
          left: barModels[selectedIndex].activeIconPosX,
          child: Container(
              height: 10,
              width: 10,
              decoration: BoxDecoration(
                  color: const Color(0xFF079AEC), shape: BoxShape.circle)),
        )
      ],
    );
  }

  double getAppropriateSizeValue(int index) {
    if (selectedIndex == previousIndex) {
      return index == selectedIndex ? MAX_LIFT_SIZE : 0;
    } else if (index == selectedIndex)
      return _selectedSizeAnimation.value;
    else if (index == previousIndex)
      return _unSelectedSizeAnimation.value;
    else
      return 0;
  }

  double getAppropriateRotationValue(index) {
    if (selectedIndex != previousIndex &&
        (index == selectedIndex || index == previousIndex)) {
      if (index < barModels.length / 2)
        return _firstHalfRotationAnimation['rotation'].value;
      else
        return _secondHalfRotationAnimation['rotation'].value;
    } else
      return 0;
  }

  void onNavTap(int newIndex) {
    setState(() {
      previousIndex = selectedIndex;
      selectedIndex = newIndex;
      for (int i = 0; i < barModels.length; i++) {
        var barModel = barModels[i];
        barModel.onBarIndexChange(i == selectedIndex);
        _controller.reset();
        _controller.forward();
      }
    });
  }
}

class BarItemIcon extends StatelessWidget {
  final IconData _unSelectedIcon;
  final IconData _selectedIcon;
  final double _selectedIconOpacity;
  final double _unSelectedIconOpacity;
  final GlobalKey _iconPosKey;
  final double _iconRotation;
  final double _liftSize;

  BarItemIcon(
      {@required IconData unSelectedIcon,
      @required IconData selectedIcon,
      @required double selectedIconOpacity,
      @required double unSelectedIconOpacity,
      @required GlobalKey iconPosKey,
      @required double iconRotation,
      @required double liftSize})
      : _unSelectedIcon = unSelectedIcon,
        _selectedIcon = selectedIcon,
        _unSelectedIconOpacity = unSelectedIconOpacity,
        _selectedIconOpacity = selectedIconOpacity,
        _iconPosKey = iconPosKey,
        _iconRotation = iconRotation,
        _liftSize = liftSize;

  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: _iconRotation,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          RectGetter(
            key: _iconPosKey,
            child: Stack(
              children: <Widget>[
                Opacity(
                  child: Icon(_unSelectedIcon),
                  opacity: _unSelectedIconOpacity,
                ),
                Opacity(
                  child: Icon(_selectedIcon),
                  opacity: _selectedIconOpacity,
                )
              ],
            ),
          ),
          SizedBox(
            height: _liftSize,
          ),
        ],
      ),
    );
  }
}

final barModels = <TabBarItemModel>[
  TabBarItemModel(OMIcons.home, Icons.home, "Home"),
  TabBarItemModel(OMIcons.cameraAlt, Icons.camera_alt, "Camera"),
  TabBarItemModel(OMIcons.insertDriveFile, Icons.insert_drive_file, "Files"),
  TabBarItemModel(OMIcons.person, Icons.person, "Profile"),
];
