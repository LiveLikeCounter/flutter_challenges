import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Test(),
    );
  }
}

class Test extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: ShareRow(),
        ),
      );
}

class ShareRow extends StatefulWidget {
  @override
  _ShareRowState createState() => _ShareRowState();
}

class _ShareRowState extends State<ShareRow>
    with SingleTickerProviderStateMixin {
  int selected = -1;
  AnimationController _animationController;
  Animation<double> _shareContainerPerspective;
  Animation<double> _socialPerspective;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(duration: Duration(milliseconds: 500), vsync: this);

    _shareContainerPerspective = Tween<double>(begin: 2 * pi, end: 3 * pi / 2)
        .animate(CurvedAnimation(
            parent: _animationController, curve: Interval(0, .5)));
    _socialPerspective = Tween<double>(begin: pi / 2, end: 0).animate(
        CurvedAnimation(
            curve: Curves.decelerate,
            reverseCurve: Curves.fastOutSlowIn,
            parent: _animationController));
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) {
        return GestureDetector(
          child: Stack(
            children: <Widget>[
              Transform(
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateX(_socialPerspective.value),
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                          child: ToggleButton(
                            iconName: "facebook",
                            isSelected: selected == 0,
                          ),
                          onTap: () {
                            setState(() {
                              selected = 0;
                            });
                          },
                          onDoubleTap: () {
                            selected = -1;
                            _animationController.reverse();
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        child: ToggleButton(
                          iconName: "twitter",
                          isSelected: selected == 1,
                        ),
                        onTap: () {
                          setState(() {
                            selected = 1;
                          });
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        child: ToggleButton(
                          iconName: "instagram",
                          isSelected: selected == 2,
                        ),
                        onTap: () {
                          setState(() {
                            selected = 2;
                          });
                        },
                      ),
                    )
                  ],
                ),
              ),
              Transform(
                transform: Matrix4.identity()
                  ..setEntry(3, 2, 0.001)
                  ..rotateX(_shareContainerPerspective.value),
                alignment: Alignment.topCenter,
                child: Container(
                  height: 75,
                  width: 200,
                  color: Colors.black,
                  child: Center(
                    child: Text(
                      "SHARE",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 35,
                          letterSpacing: 3),
                    ),
                  ),
                ),
              )
            ],
          ),
          onTap: () {
            _animationController.forward();
          },
        );
      },
    );
  }
}

class ToggleButton extends StatelessWidget {
  final bool isSelected;
  final String iconName;

  ToggleButton({this.isSelected, this.iconName});

  @override
  Widget build(BuildContext context) => IndexedStack(
        index: isSelected ? 1 : 0,
        children: <Widget>[
          Image.asset("assets/grey/$iconName-icon.png"),
          Image.asset("assets/selected/$iconName-icon.png"),
        ],
      );
}