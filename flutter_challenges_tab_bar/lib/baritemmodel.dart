import 'package:flutter/cupertino.dart';
import 'package:rect_getter/rect_getter.dart';

class TabBarItemModel {
  final IconData unSelectedIcon;
  final IconData selectedIcon;
  final String _text;
  double activeIconPosX = 0;
  final GlobalKey iconPosKey = RectGetter.createGlobalKey();

  double selectedIconOpacity = 0;
  double unSelectedIconOpacity = 0;
  double textOpacity = 1;

  TabBarItemModel(this.unSelectedIcon, this.selectedIcon, this._text);

  void onBarIndexChange(bool isSelected) {
    if (isSelected) {
      selectedIconOpacity = 1;
      unSelectedIconOpacity = 0;
      textOpacity = 0;
      updateActiveIconPosition();
    } else {
      selectedIconOpacity = 0;
      unSelectedIconOpacity = 1;
      textOpacity = 1;
    }
  }

  void updateActiveIconPosition() {
    Rect rect = RectGetter.getRectFromKey(iconPosKey);

    activeIconPosX = rect.center.translate(-5, 0).dx;
  }

  Widget get textWidget => AnimatedOpacity(
        child: Text(_text),
        opacity: textOpacity,
        duration: const Duration(milliseconds: 200),
      );
}
